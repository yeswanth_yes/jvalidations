(function( $ ){

		$.fn.validateForm3 = function(options){
		
			var settings= new Array();
			for(field in options)
			{
				settings[field] = $.extend( {
					mandatory:false,
					minlength: false,
					maxlength: false,		
					alphabets: false,
					signedNumeric: false,
					unSignedNumeric:false,
					signedFloat: false,
					unSignedFloat:false,
					alphaNumeric: false,
					email: false,
					startWith: false,
					endWith: false,
					shouldHave: false,
					notHave: false,
					groupMandate: false,
					errorTitle: "The above field",
					customMessage:false ,
					customValidationRequired:false,
					errorSpan:"<span class='help-block' id='"+thisID+"Error'>&nbsp;</span>"
			    }, options[field]);
			 
			 	var thisID=$(field).attr("id");
			 	if(settings[field]['errorSpan']!=false)
			 	{
			 		$(field).after(settings[field]['errorSpan']);
			 	}
			 	
			 	if(settings[field].signedNumeric)
			    {
			    	$(field).numeric(false);
			    }
			    else if(settings[field].unSignedNumeric)
			    {
			    	$(field).numeric({ decimal: false, negative: false });
			    }
			    else if(settings[field].signedFloat)
			    {
			    	$(field).numeric();
			    }
			    else if(settings[field].unSignedFloat)
			    {
			    	$(field).numeric({ negative: false });
			    }
			    
			 }
			 
			 $(this).submit(function(){
				
				var check=0;
				var commonFormErrors="";
				
				for(field in settings)
				{
					var value=$(field).val();
					var valueTrimmed=value.replace(/^\s+|\s+$/g,'');
					var length=valueTrimmed.length;
					var fieldCheck=0;
					var errorMessage="";
					
					if(settings[field].mandatory!==false)
				    {
				    	if(length<=0)
				    	{
				    		errorMessage=settings[field].errorTitle+" should not be empty";				    		
				    		check=1;
				    		fieldCheck=1;
				    	}
				    }
				    if(settings[field].minlength!==false && fieldCheck==0 && length!=0)
				    {
				    	if(length<settings[field].minlength)
				    	{
				    		errorMessage=settings[field].errorTitle+"  should have minimum of "+settings[field].minlength+" characters ";
				    		check=1;
				    		fieldCheck=1;
				    	}
				    }
				    if(settings[field].maxlength!==false && fieldCheck==0 && length!=0)
				    {
				    	if(length>settings[field].maxlength)
				    	{
				    		errorMessage=settings[field].errorTitle+"  should have maximum of "+settings[field].maxlength+" characters ";				    		
				    		check=1;
				    		fieldCheck=1;
				    	}
				    }
				    if(settings[field].email!==false && fieldCheck==0 && length!=0)
				    {
				    	var email_regex=/^[\w]+[\w\.\-]+[\w]@([\w\-]+\.)+[a-zA-Z]+$/;
				    	if(!email_regex.test(valueTrimmed))
						{
							errorMessage="It is not a valid email address";							
							check=1;
							fieldCheck=1;
						}						
				    }				    					
				    if(settings[field].startWith!==false && fieldCheck==0 && length!=0)
				    {
				    	var preStart='^/'+escape(settings[field].startWith);				    	
				    	var startRegex=new RegExp(preStart, 'gi');
				    	if(!startRegex.test(valueTrimmed))
				    	{				    		
				    		errorMessage=settings[field].errorTitle+"  should start with "+settings[field].startWith;				    		
							check=1;
							fieldCheck=1;
				    	}				    	
				    }    
				    if(settings[field].endWith!==false && fieldCheck==0 && length!=0)
				    {
				    	var preEnd='$/'+escape(settings[field].endWith);				    	
				    	var endRegex=new RegExp(preStart, 'gi');
				    	if(!endRegex.test(valueTrimmed	))
				    	{
				    		errorMessage=settings[field].errorTitle+"  should end with "+settings[field].endWith;				    		
							check=1;
							fieldCheck=1;
				    	}	
				    }				    
				    if(settings[field].shouldHave!==false && fieldCheck==0 && length!=0)
				    {
				    	var preHave=settings[field].shouldHave;
				    	var haveRegex=new RegExp(preHave, 'gi');
				    	if(!haveRegex.test(valueTrimmed))
				    	{
				    		errorMessage=settings[field].errorTitle+"  should have atleast 1 "+settings[field].shouldHave;				    		
							check=1;
							fieldCheck=1;
				    	}				    	
				    }				    
				    if(settings[field].notHave!==false && fieldCheck==0 && length!=0)
				    {
				    	var preNot=settings[field].notHave;
				    	var notRegex=new RegExp(preNot,'gi');				    
				    	if(notRegex.test(valueTrimmed))
				    	{
				    		errorMessage=settings[field].errorTitle+"  should not have "+settings[field].notHave;				    		
							check=1;
							fieldCheck=1;
				    	}
				    }
				    if(settings[field].notEqual!==false && fieldCheck==0 && length!=0)
				    {
				    	var notEqual=settings[field].notEqual;
				    					    
				    	if(notEqual==valueTrimmed)
				    	{
				    		errorMessage=settings[field].errorTitle+"  should not be equal to "+settings[field].notHave;				    		
							check=1;
							fieldCheck=1;
				    	}
				    }				    
				    if(settings[field].groupMandate!==false)
				    {
				    	var groupCheck=0;
				    	$(this).find(field).each(function(){
				    		if($(this).attr("checked"))
				    		{
				    			groupCheck=1;
				    		}
				    	});
				    	
				    	if(!groupCheck)
				    	{
				    		commonFormErrors+="Atleast one of the "+settings[field].errorTitle+" should be selected.";
				    		check=1;
				    		fieldCheck=1;
				    	}
				    }
				    if(settings[field].customValidationRequired!=false && fieldCheck==0)
				    {
				    	var customResult=settings[field].customValidation();
				    	if(customResult!=1)
				    	{
				    		errorMessage=customResult;
				    		check=1;
				    		fieldCheck=1;
				    	}
				    }
				    
				    if(fieldCheck==0)
					$(field+"Error").html("");
					
					(settings[field].customMessage)?$(field+"Error").html(settings[field].customMessage):$(field+"Error").html(errorMessage);
				
				}
				
				
				if(check==0)
				return true;
				else
				return false;
				
			});
			 
			 
		
		};
		
})( jQuery );